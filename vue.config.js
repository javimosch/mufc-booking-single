module.exports = {
  runtimeCompiler: true,
  devServer: {
    disableHostCheck: true,
    host: "0.0.0.0",
    proxy: {
      "/api": {
        target: "http://localhost:3001",
        pathRewrite: { "^/api": "" },
        secure: false,
      },
      "/admin": {
        target: "http://localhost:3001",
        secure: false,
      },
    },
  },
};
