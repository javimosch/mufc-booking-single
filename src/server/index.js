require("dotenv").config({ silent: true });
const express = require("express");
const app = express();
const PORT = process.env.PORT || 8080;
const path = require("path");

const initialize = async () => {
  const app = express();
  const savoieMiddleware = require("./savoie/express-middleware.js");

  await savoieMiddleware(app, {
    bodyParser: true,
  });

  await require("./module.js")(app, {
    name: "mufc",
    getPath: (relativePath) => path.join(__dirname, relativePath),
    getRouteName: (name) => path.join("/", name),
  });

  app.listen(PORT, () => console.log(`Listening at :${PORT}`));
};

initialize();
